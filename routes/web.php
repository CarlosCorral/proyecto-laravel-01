<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\InicioController;
use App\Http\Controllers\EncuestaController;
use App\Http\Controllers\CandidatoController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//PAgina principal * 
Route::get('',[InicioController::class,'inicio']);
Route::get('encuestas',[EncuestaController::class,'index'])->name("encuestas.index");
//ver tu perfil y si tiene encuesta *
Route::get('encuestas/perfil',[EncuestaController::class,'mostrarPerfil'])->name('encuestas.perfil')->middleware('auth');

//Crear tu encuesta (solo desde el perfil)*
Route::get('encuestas/perfil/crear',[EncuestaController::class,"crear"])->name('encuestas.create')->middleware('auth');
//Añadir los datos de la encuesta *
Route::post('encuestas/perfil/crear',[EncuestaController::class,"store"])->name('encuestas.store')->middleware('auth');
//Crear candidato*
Route::get('encuestas/perfil/candidatos/crear',[CandidatoController::class,"create"])->name('candidatos.create')->middleware('auth');
//Añadir los datos del candidato*
Route::post('encuestas/perfil/candidatos/crear',[CandidatoController::class,"store"])->name('candidatos.store')->middleware('auth');
//Modificar candidato
Route::get('encuestas/perfil/{candidato}/modificar',[CandidatoController::class,"edit"])->name('candidatos.edit')->middleware('auth');
//Añadir modificación
Route::put('encuestas/perfil/{candidato}/modificar', [CandidatoController::class,"update"])->name('candidatos.update')->middleware('auth');
//Votar
Route::get('/encuestas/voto/{candidato}',[CandidatoController::class,"añadirVoto"])->name('candidatos.vote')->middleware('auth');
//ver encuesta*
Route::get('encuestas/{encuesta}',[EncuestaController::class,'mostrarEncuesta'])->name('encuestas.show');

//eliminar encuesta
Route::get('encuestas/perfil/delete',[EncuestaController::class,'eliminarEncuesta'])->name('encuestas.delete')->middleware('auth');





