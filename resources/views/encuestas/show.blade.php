@extends('layouts.master')
@section('contenido')
<div class="row">
    <div class="col-lg-12">
        <!-- Bloque -->
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="display-1">{{$encuesta->nombre}}</h3>
                </div>
            </div>
            <div class="card-body">
                @if($encuesta->imagen!=null)
                <div style="float:left; padding-right:20px;">
                    <img src="{{asset('assets/imagenes')}}/{{$encuesta->imagen}}" height="300px">
                </div>
                @endif
                <div class="pull-right">
                    <h3 class="display-5">{{ $encuesta->descripcion }}</h3>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        @foreach($candidatos as $candidato)
                        <tr>
                            <td>
                                @if(($candidato->imagen)!=null)
                                <img src="{{asset('assets/imagenes')}}/{{$candidato->imagen}}" height="100px">
                                @else
                                No hay imagen
                                @endif
                            </td>
                            <td>
                                {{$candidato->nombre}}
                            </td>
                            <td>
                                @if(($candidato->descripcion)!=null)
                                <p class="class=" font-weight-boldest>{{$candidato->descripcion}}</p>
                                @else
                                No hay descripción
                                @endif
                            </td>
                            <td>
                                Votos: {{count($candidato->votos)}}<br>
                                <a class="btn btn-primary" href="{{route('candidatos.vote',$candidato)}}" role="button">Votar</a>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
                <a class="btn btn-primary" href="{{route('encuestas.index')}}" role="button">Volver</a>
            </div>
        </div>
        <!-- end bloque -->
    </div>
</div>
@endsection