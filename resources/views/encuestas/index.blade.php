@extends('layouts.master')
@section('contenido')


@foreach($encuestas as $encuesta)
<a href="{{ route('encuestas.show' , $encuesta ) }}">
<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <h2 class="card-label">{{$encuesta->nombre}}</h2>
                </div>
            </div>
            @if($encuesta->descripcion)
            <div class="card-body">
                {{$encuesta->descripcion}}
            </div>
            @endif
        </div>
    </div>
</div>
</a>
<br>
@endforeach

@endsection