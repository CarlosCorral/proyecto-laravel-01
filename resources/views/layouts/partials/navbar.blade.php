<div id="kt_header" class="header header-fixed">
	<div class="container-fluid d-flex align-items-stretch justify-content-between">
		<div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
			<div class="header-logo">
				<a href="{{route('encuestas.index')}}">
					<img alt="Logo" src="{{ url('assets/media/logos/polls.png') }}" width="150px" />
				</a>
				<a href="{{route('encuestas.perfil')}}">
				<button type="button" class="btn btn-success" style="margin-left:40px;">Perfil</button>
				</a>
			</div>
		</div>
	</div>
</div>