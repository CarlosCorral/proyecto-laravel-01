@extends('layouts.master')
@section('contenido')
<div class="row">
    <div class="col-lg-12">
        <!-- Bloque -->
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                <form method="post" action="{{route('encuestas.store')}}" encType="multipart/form-data" >
                    <h3 class="display-1">{{$candidato->nombre}}</h3>
                </div>
            </div>
            <div class="card-body">
                @if($candidato->imagen!=null)
                <div style="float:left; padding-right:20px;">
                    <img src="{{asset('assets/imagenes')}}/{{$candidato->imagen}}" height="300px">
                </div>
                @endif
                <div class="pull-right">
                    <h3 class="display-5">{{ $candidato->descripcion }}</h3>
                </div>
                </form>
                <a class="btn btn-primary" href="{{route('encuestas.index')}}" role="button">Volver</a>
            </div>
        </div>
        <!-- end bloque -->
    </div>
</div>
@endsection