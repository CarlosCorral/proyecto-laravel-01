@extends('layouts.master')
@section('contenido')
<div class="row">
	<div class="col-lg-12">

		<!-- Bloque -->

		<div class="card card-custom">
			<div class="card-header">
				<h3 class="display-3">Crear candidato</h3>
			</div>
			<div class="card-body">
				<form method="POST" action="{{route('candidatos.update',$candidato)}}" encType="multipart/form-data">
					@csrf
					@method('put')
					<div class="card-title">
						<h3 class="display-5">Nombre del candidato:</h3>
						<input type="text" name="nombre" id="nombre" class="form-control" value="{{$candidato->nombre}}" required>
					</div>
					<div class="card-title">
						<h3 class="display-5">Descripción:</h3><br>
						<textarea id="descripcion" name="descripcion" rows="10" cols="40">"{{$candidato->descripcion}}"</textarea>
					</div>
					<div class="card-title">
						<h3 class="display-5">Imagen:</h3>
						<input type="File" name="imagen" id="imagen" class="form-control">
					</div>
					<button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">
						Añadir Candidato
					</button>
				</form>
			</div>
		</div>
		<!-- end bloque -->
	</div>
</div>
@endsection