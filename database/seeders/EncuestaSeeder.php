<?php

namespace Database\Seeders;
use App\Models\Encuesta;
use Illuminate\Database\Seeder;

class EncuestaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $encuesta = new Encuesta();
        $encuesta->nombre='Mejor Actor';
        $encuesta->user_id='1';
        $encuesta->descripcion='Estamos viviendo una época dorada de la actuación, de hecho, de platino. A esa conclusión llegamos cuando decidimos seleccionar a nuestros actores favoritos de cine de los últimos 20 años. No existe una fórmula para elegir a los mejores (solo muchas discusiones), y esta lista es inevitablemente subjetiva y quizá escandalosa por sus omisiones. Algunos de estos actores son noveles; otros han estado en el medio desde hace décadas. Para nuestra selección, nos enfocamos en este siglo y consideramos talentos más allá de Hollywood. Y, aunque sin duda hay estrellas en la lista e incluso unos cuantos ganadores del Oscar, también hay actores de reparto y camaleones, héroes de acción y tesoros del cine de arte. Ellos representan 25 razones por las que todavía amamos las películas, quizá hoy más que nunca.';
        $encuesta->imagen='RyanReynolds.jpg';
        $encuesta->save();

        $encuesta2 = new Encuesta();
        $encuesta2->nombre='Mejor Serie de Animación';
        $encuesta2->user_id='2';
        $encuesta2->save();
    }
}
