<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        $this->call(UserSeeder::class);

        DB::table('encuestas')->delete();
        $this->call(EncuestaSeeder::class);

        DB::table('candidatos')->delete();
        $this->call(CandidatoSeeder::class);
    }
}
