<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Candidato;
class CandidatoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $encuesta = new Candidato();
        $encuesta->nombre='Johnny Depp';
        $encuesta->encuesta_id='1';
        $encuesta->descripcion='Es un actor, productor, guionista y músico estadounidense. Ha sido nominado en tres ocasiones al Óscar y recibió un Globo de Oro, ​ un premio del Sindicato de Actores y un César.​';
        $encuesta->imagen='johnnyDepp.jpg';
        $encuesta->save();

        $encuesta2 = new Candidato();
        $encuesta2->nombre='Brad Pitt';
        $encuesta2->encuesta_id='1';
        $encuesta2->save();
    }
}
