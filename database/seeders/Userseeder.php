<?php

namespace Database\Seeders;
use App\Models\User;
use Illuminate\Database\Seeder;

class Userseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usuario = new User();
        $usuario->name='carlos';
        $usuario->email='carlos@gmail.com';
        $usuario->password=bcrypt('carlos');
        $usuario->save();

        $usuario2 = new User();
        $usuario2->name='Javier';
        $usuario2->email='javier@gmail.com';
        $usuario2->password=bcrypt('javier');
        $usuario2->save();
    }
}
