<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Candidato;
use App\Models\Voto;
use App\Models\Encuesta;
use Illuminate\Support\Facades\Auth;
use PhpParser\Node\Expr\FuncCall;

class CandidatoController extends Controller
{
    //Comprobar que funciona los votos
    public function añadirVoto(Candidato $candidato){
        $datos["user_id"]=Auth::id();
        $datos["candidato_id"]=$candidato->id;
        $votoExiste=Voto::find($datos["user_id"],$datos["candidato_id"]);
       if($votoExiste!=null){
        Voto::create($datos);
       }
        return redirect()->route("encuestas.index")->with('mensajeError', 'Ha habido un error');
    }

    //Te lleva a la pagina para crear al candidato*
    public function create(){
        return view('candidatos.create');
    }
    //Para crear un candidato*
    public function store(Request $request){
        $datos= $request->all();
        $hayEncuesta=Encuesta::all()->where("user_id",Auth::id())->first();
        if($hayEncuesta==null){
            return redirect()->route("encuestas.perfil")->with('mensajeError', 'Ha habido un error');
            }   
        $datos['encuesta_id']=$hayEncuesta->id;
        Candidato::create($datos);
        return redirect()->route("encuestas.perfil")->with('mensajeError', 'Ha habido un error');
    }

    
 
    public function edit(Candidato $candidato){
        return view('candidatos.edit',['candidato'=>$candidato] );  
    }
    //borrar encuesta
    public function eliminarEncuesta(Encuesta $encuesta){
        $candidatos=Candidato::all()->where("encuesta_id",$encuesta->id);
        foreach($candidatos as $candidato){
            $votos=Voto::all()->where("candidato_id",$candidato->id);
            foreach($votos as $voto)
                $voto->delete();
            $candidato->delete();
        }
        $encuesta->delete();
        return view('encuestas.perfil');
    }
  
    public function update(Request $request,Candidato $candidato){
        $candidato->nombre=$request->nombre;
        $candidato->imagen=$request->imagen;
        $candidato->descripcion=$request->descripcion;
        $candidato->save();
        return redirect()->route('encuestas.perfil');
      }
}
