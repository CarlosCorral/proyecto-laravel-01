<?php

namespace App\Http\Controllers;
use App\Models\Encuesta;
use App\Models\Candidato;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Voto;
use App\Models\User;
class EncuestaController extends Controller
{
    //EL index*
    public function index(){
        $encuestas= Encuesta::all();
		return view('encuestas.index',["encuestas"=>($encuestas)]);
		//return view('animales.index',compact("animales"));
    }
    // Muestra la encuesta detenidamente*
    public function mostrarEncuesta(Encuesta $encuesta){
        $candidatos=Candidato::all()->where("encuesta_id",$encuesta->id);
        return view("encuestas.show",["encuesta"=>$encuesta],["candidatos"=>$candidatos]);
    }
    // Devuelve los datos del perfil*
    public function mostrarPerfil(){
        $usuario = Auth::id();
        $encuesta=Encuesta::all()->where("user_id",$usuario)->first();
        $candidatos=Candidato::all()->where("encuesta_id",$encuesta->id);
        return view("encuestas.perfil",["encuesta"=>$encuesta],["candidatos"=>$candidatos]);
    }
    //Te lleva a la pagina para crear la encuesta*
    public function crear(){
        return view('encuestas.create');
    }
   
   
    //Para crear una ecuesta, si ya hay una creada la elimina*
    public function store(Request $request){
        $datos= $request->all();
        $datos['user_id']=Auth::id();
        $hayEncuesta=Encuesta::all()->where("user_id",Auth::id())->first();
        if($hayEncuesta!=null){
            $candidatos=Candidato::all()->where("encuesta_id",$hayEncuesta->id);
            if($candidatos!=null){
                foreach($candidatos as $candidato){
                    $candidato->delete();
                }
            }
            $hayEncuesta->delete();
        }
        Encuesta::create($datos);
        return redirect()->route("encuestas.perfil")->with('mensajeError', 'Ha habido un error');
    }

    //borrar encuesta
    public function eliminarEncuesta(Encuesta $encuesta){
        $candidatos=Candidato::all()->where("encuesta_id",$encuesta->id);
        foreach($candidatos as $candidato){
            $votos=Voto::all()->where("candidato_id",$candidato->id);
            foreach($votos as $voto)
                $voto->delete();
            $candidato->delete();
        }
        $encuesta->delete();
        $encuesta=null;
        return view('encuestas.perfil',["encuesta"=>$encuesta]);
    }

}
