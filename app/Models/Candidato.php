<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Encuesta;
class Candidato extends Model
{
    protected $table="candidatos";
    protected $guarded=[];

    public function encuestas()
    {
    return $this->belongsToMany(Encuesta::class);
    }

    public function votos()
    {
    return $this->hasMany(Voto::class);
    }
}
