<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Candidato;
class Encuesta extends Model
{
    protected $table="encuestas";
    protected $guarded=[];  
    
    public function candidatos()
    {
    return $this->hasMany(Candidato::class);
    }
}
