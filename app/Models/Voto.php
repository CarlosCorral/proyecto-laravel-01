<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Voto extends Model
{
    protected $table="votos";
    protected $guarded=[];

    public function candidato(){
        return $this->belongsTo(Candidato::class);
    }
}
